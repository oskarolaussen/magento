<?php
namespace Survey\SurveyPage\Api\Data;
interface AnswerInterface
{
   const ANSWER_ID = 'answer_id';
   const RATING = 'rating';
   const MESSAGE1 = 'message1';
   const PRODUCT_ID = 'product_id';
   const MESSAGE2 = 'message2';

   public function getId();
   public function getRating();
   public function getMessage1();
   public function getMessage2();
   public function getProductId();
   public function setId($id);
   public function setRating($rating);
   public function setMessage1($message1);
   public function setMessage2($message2);
   public function setProductId($product_id);
}
